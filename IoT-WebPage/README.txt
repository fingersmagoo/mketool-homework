Due to Chrome having CORS issues,  create a shortcut to disable them
"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-web-security --user-data-dir="c:/chromedev"