var awsIot = require('aws-iot-device-sdk');

var device = awsIot.device({
    keyPath: 'a2e740b118-private.pem.key',
    certPath: 'a2e740b118-certificate.pem.crt',
    caPath: 'rootCA.pem',
    clientId: 'MKEToolIoTDevice',
    region: 'us-east-2',
    host: 'a1zc0bhyih0o7z.iot.us-east-2.amazonaws.com'
});

var data = {"device":"dev-123","temp":800,"humidity":35,"pressure":389,"voltage":110};

device.on('connect', function(){
    console.log('connect');
    device.publish('MKEToolTopic', JSON.stringify(data));
    console.log('Message Sent');
});

device.on('message', function(topic, payload){
    console.log('message', topic, payload.toString());
});
