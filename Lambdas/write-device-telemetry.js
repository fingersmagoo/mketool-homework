'use strict';

var AWS = require('aws-sdk'),
	uuid = require('uuid'),
	documentClient = new AWS.DynamoDB.DocumentClient(); 

exports.handler = function(event, context, callback){
	var params = {
		Item : {
			"id" : uuid.v1(),
			"device" : event.device,
			"temp" : event.temp,
			"humidity" : event.humidity,
			"pressure" : event.pressure,
			"voltage" : event.voltage,
			"timestamp" : new Date(Date.now()).toString()
		},
		TableName : process.env.TABLE_NAME
	};
	documentClient.put(params, function(err, data){
		callback(err, data);
	});
	
	if(event.temp >500) {
		console.log('TOO HOT!');
		var alertParams = {
		Item : {
			"id" : uuid.v1(),
			"device" : event.device,
			"alertType": "temp",
			"temp" : event.temp,
			"timestamp" : new Date(Date.now()).toString()
		},
		TableName : process.env.ALERT_TABLE
		};
		documentClient.put(alertParams, function(err, data){
			callback(err, data);
		});
	}
	
}
